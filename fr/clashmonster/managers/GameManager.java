package fr.clashmonster.managers;

import fr.clashmonster.ClashMonster;
import fr.clashmonster.utils.ItemBuilder;
import fr.clashmonster.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class GameManager {

    public static boolean isStarted;
    public static List<String> blockedPlayerStart = new ArrayList<>();

    public static void startGame() {

        isStarted = true;

        Bukkit.broadcastMessage(Utils.LINE);
        Bukkit.broadcastMessage("§eLe jeu se lancera dans 5 secondes !");
        Bukkit.broadcastMessage(Utils.LINE);

        Bukkit.getScheduler().runTaskLater(ClashMonster.getInstance(), new Runnable() {
            @Override
            public void run() {

                Bukkit.broadcastMessage(Utils.LINE);
                Bukkit.broadcastMessage("§eBonne chance ! Que le meilleur gagne !");
                Bukkit.broadcastMessage(Utils.LINE);
                blockedPlayerStart.clear();

            }
        }, 100L);
    }

    public void endGame() {

        isStarted = false;

        Bukkit.broadcastMessage(Utils.LINE);
        Bukkit.broadcastMessage("§eLe jeu vient de se terminer !");
        Bukkit.broadcastMessage(Utils.LINE);

    }

    public static void teleportPlayer(Player player1, Player player2) {

        player1.teleport(new Location(Bukkit.getWorld("world"), 100,80,0,0,0));
        player2.teleport(new Location(Bukkit.getWorld("world"), 100,80,50,0,0));
    }

    public static void giveItems(Player player) {

        player.getInventory().clear();

        ItemBuilder fire = new ItemBuilder(Material.MAGMA_CREAM).setName("§6Effet de feu").setDurability((short) 6);
        ItemBuilder snowball = new ItemBuilder(Material.SNOWBALL).setName("§fEffet de glace").setDurability((short) 6);
        ItemBuilder poison = new ItemBuilder(Material.SPIDER_EYE).setName("§fEffet de Poison").setDurability((short) 6);
        ItemBuilder damage = new ItemBuilder(Material.IRON_INGOT).setName("§fDommage instantané").setDurability((short) 3);

        player.getInventory().setItem(0, fire.toItemStack());
        player.getInventory().setItem(2, snowball.toItemStack());
        player.getInventory().setItem(4, poison.toItemStack());
        player.getInventory().setItem(6, damage.toItemStack());
    }

}
