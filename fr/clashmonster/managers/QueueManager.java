package fr.clashmonster.managers;

import fr.clashmonster.ClashMonster;
import fr.clashmonster.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class QueueManager {

    public static List<String> playerInQueue = new ArrayList<>();

    public static void checkQueue() {

        Bukkit.getScheduler().scheduleSyncRepeatingTask(ClashMonster.getInstance(), new Runnable() {

            @Override
            public void run() {

                if(playerInQueue.size() == 2 && GameManager.isStarted == false) {

                    Player player1 = Bukkit.getPlayer(playerInQueue.get(0));
                    Player player2 = Bukkit.getPlayer(playerInQueue.get(1));

                    GameManager.teleportPlayer(player1, player2);

                    player1.sendMessage(Utils.LINE);
                    player1.sendMessage("§eVous rejoingnez une partie !");
                    player1.sendMessage(Utils.LINE);

                    player2.sendMessage(Utils.LINE);
                    player2.sendMessage("§eVous rejoingnez une partie !");
                    player2.sendMessage(Utils.LINE);

                    playerInQueue.clear();

                    GameManager.startGame();
                }
            }
        }, 20, 250);
    }
}
