package fr.clashmonster.profiles;

import fr.clashmonster.utils.Utils;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.List;

public class PlayerProfile {

    private String name;
    private int win;
    private int loose;
    private int playedParty;
    private double money;

    public PlayerProfile(String name, int win, int loose, int playedParty, double money) {
        this.name = name;
        this.win = win;
        this.loose = loose;
        this.playedParty = playedParty;
        this.money = money;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public int getLooses() {
        return loose;
    }

    public void setLooses(int loose) {
        this.loose = loose;
    }

    public void setWins(int win) {
        this.win = win;
    }

    public int getWins() {
        return win;
    }

    public double getMoney() {
        return money;
    }

    public int getPlayedParty() { return playedParty; }

    public void setPlayedParty(int playedParty) { this.playedParty = playedParty; }

    public File getProfileFile() {
        return Utils.getFormatedFile("/players/" + this.name + ".json");
    }

}
