package fr.clashmonster.profiles;

import com.google.common.collect.Lists;
import fr.clashmonster.ClashMonster;
import fr.clashmonster.utils.FileUtils;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.List;

public class PlayerProfileManager {

    private static PlayerProfileManager INSTANCE;
    private List<PlayerProfile> profiles;

    public PlayerProfileManager() {

        INSTANCE = this;
        this.profiles = Lists.newArrayList();
    }

    public PlayerProfile getProfile(String value) {
        for (PlayerProfile profile : this.profiles) {
            if (!profile.getName().equalsIgnoreCase(value)) continue;
            return profile;
        }
        return null;
    }

    //@Override
    public void loadData(String player) {

        String json = FileUtils.loadFile(new File(ClashMonster.getInstance().getDataFolder() + "/players", player + ".json"));
        PlayerProfile profile = (PlayerProfile) ClashMonster.getInstance().serialize.deserialize(json, PlayerProfile.class);

        if(profile == null)
            profile = new PlayerProfile(player, 0, 0, 0, 0.0);

        profiles.add(profile);
    }

    public void createProfile(PlayerProfile profile) {
        if (this.getProfile(profile.getName()) == null) {
            this.profiles.add(profile);
        }
    }

    public void createProfile(String name, int win, int loose, int playerParty, double money) {
        PlayerProfile profile = new PlayerProfile(name, win, loose, playerParty, money);
        this.createProfile(profile);
    }

    public void saveData(String player) {

        PlayerProfile profile = this.getProfile(player);
        String json = ClashMonster.getInstance().serialize.serialize(profile);

        FileUtils.saveFile(new File(ClashMonster.getInstance().getDataFolder() + "/players/", player + ".json"), json);

        profiles.remove(profile);
    }

    public static PlayerProfileManager getInstance() {
        return INSTANCE;
    }

    public List<PlayerProfile> getProfiles() {
        return this.profiles;
    }
}
