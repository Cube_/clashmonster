package fr.clashmonster.utils;

import fr.clashmonster.ClashMonster;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;

import java.io.File;
import java.util.Random;

public class Utils {

    public static String LINE = Utils.color("&7&m" + StringUtils.repeat((String)"-", (int)53));
    public static String SCOREBOARD_LINE = Utils.color("&7&m" + StringUtils.repeat((String)"-", (int)20));

    public static String color(String msg) {
        return ChatColor.translateAlternateColorCodes((char)'&', (String)msg);
    }

    public static File getFormatedFile(String fileName) {
        return new File(ClashMonster.getInstance().getDataFolder(), fileName);
    }

    public static int randomInt(int min, int max) {
        Random random = new Random();
        int range = max - min + 1;
        int randomNum = random.nextInt(range) + min;
        return randomNum;
    }

    public static boolean isInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
