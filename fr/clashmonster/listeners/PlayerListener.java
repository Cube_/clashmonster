package fr.clashmonster.listeners;

import fr.clashmonster.profiles.PlayerProfileManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    private PlayerProfileManager profileManager;

    public PlayerListener() {

        this.profileManager = PlayerProfileManager.getInstance();
    }

    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {

        this.profileManager.loadData(event.getPlayer().getName());
    }

    @EventHandler
    public void playerLeave(PlayerQuitEvent event) {

        this.profileManager.saveData(event.getPlayer().getName());
    }


}
