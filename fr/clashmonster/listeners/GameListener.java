package fr.clashmonster.listeners;

import fr.clashmonster.managers.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class GameListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if(GameManager.blockedPlayerStart.contains(player.getName())) {

            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {

        Player player = event.getPlayer();

    }

    @EventHandler
    public void onHunger(FoodLevelChangeEvent event) {

        event.setCancelled(true);
    }

    @EventHandler
    public void onWeather(WeatherChangeEvent event) {

        if(event.toWeatherState())
            event.setCancelled(true);
    }

    @EventHandler
    public void onThunder(ThunderChangeEvent event) {

        if(event.toThunderState())
            event.setCancelled(true);
    }
}
