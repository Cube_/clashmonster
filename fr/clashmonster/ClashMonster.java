package fr.clashmonster;

import fr.clashmonster.commands.*;
import fr.clashmonster.listeners.GameListener;
import fr.clashmonster.listeners.PlayerListener;
import fr.clashmonster.managers.QueueManager;
import fr.clashmonster.profiles.PlayerProfileManager;
import fr.clashmonster.utils.Serialize;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ClashMonster extends JavaPlugin {

    public static ClashMonster instance;
    public Serialize serialize;

    /*
    /* Méthode s'éxécutant lors du démarrage du serveur
    */
    @Override
    public void onEnable() {

        instance = this;

        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("§eClashMonster §7- [§aON§7]");
        Bukkit.getConsoleSender().sendMessage("");
        this.serialize = new Serialize();
        new PlayerProfileManager();
        registerEvents();
        saveDefaultConfig();
        QueueManager.checkQueue();
    }

    /*
    /* Méthode s'éxécutant lors de l'arrêt du serveur
    */
    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage("");
        Bukkit.getConsoleSender().sendMessage("§eClashMonster §7- [§cOFF§7]");
        Bukkit.getConsoleSender().sendMessage("");

    }

    /*
    /* Méthode s'éxécutant lorsque qu'elle est appelé dans le onEnable()
    */
    public void registerEvents() {

        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new GameListener(), this);

        getCommand("money").setExecutor(new MoneyCommand());
        getCommand("play").setExecutor(new PlayCommand());
        getCommand("setspawn").setExecutor(new SetSpawnCommand());
        getCommand("setpos").setExecutor(new SetPosSpawnCommand(this));
        getCommand("hub").setExecutor(new HUBCommand());
    }


    /*
    /* Méthode permettant de récupérer l'instance de la classe principale dans les autres classes.
    */

    public static ClashMonster getInstance() {
        return instance;
    }
}
