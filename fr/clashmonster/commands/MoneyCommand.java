package fr.clashmonster.commands;

import fr.clashmonster.profiles.PlayerProfile;
import fr.clashmonster.profiles.PlayerProfileManager;
import fr.clashmonster.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MoneyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if(!(sender instanceof Player))
            return false;

        if(sender instanceof Player) {

            PlayerProfile profile = PlayerProfileManager.getInstance().getProfile(player.getName());

            player.sendMessage(Utils.LINE);
            player.sendMessage("§eVous avez : " + profile.getMoney() + "$");
            player.sendMessage(Utils.LINE);
        }

        return false;
    }
}
