package fr.clashmonster.commands;

import fr.clashmonster.managers.GameManager;
import fr.clashmonster.managers.QueueManager;
import fr.clashmonster.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if(!(sender instanceof Player))
            return false;

        if(sender instanceof Player) {

            if(args.length == 1 && args[0].equals("join")) {

                if(QueueManager.playerInQueue.contains(player.getName())) {
                    player.sendMessage(Utils.LINE);
                    player.sendMessage("§eVous êtes déja dans la file d'attente !");
                    player.sendMessage(Utils.LINE);
                } else {

                    player.sendMessage(Utils.LINE);
                    player.sendMessage("§eVous venez d'être ajouté à la liste d'attente !");
                    player.sendMessage(Utils.LINE);
                    QueueManager.playerInQueue.add(player.getName());
                }
            }

            if(args.length == 1 && args[0].equals("leave")) {

                if(!QueueManager.playerInQueue.contains(player.getName())) {

                    player.sendMessage(Utils.LINE);
                    player.sendMessage("§eVous n'êtes pas dans la file d'attente !");
                    player.sendMessage(Utils.LINE);

                } else {

                    player.sendMessage(Utils.LINE);
                    player.sendMessage("§eVous venez d'être retiré à la liste d'attente !");
                    player.sendMessage(Utils.LINE);
                    QueueManager.playerInQueue.remove(player.getName());
                }
            }

        }

        return false;
    }
}
