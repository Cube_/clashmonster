package fr.clashmonster.commands;

import fr.clashmonster.profiles.PlayerProfile;
import fr.clashmonster.profiles.PlayerProfileManager;
import fr.clashmonster.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GiveMoneyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if(!(sender instanceof Player))
            return false;

        String target = args[0];

        if(sender instanceof Player && args.length == 2 && Utils.isInt(args[1])) {

            PlayerProfile profile = PlayerProfileManager.getInstance().getProfile(target);
            profile.setMoney(Double.parseDouble(profile.getMoney() + args[1]));

        } else {

            player.sendMessage("§cMauvais arguments !");
        }

        return false;
    }
}