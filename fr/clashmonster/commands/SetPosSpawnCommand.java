package fr.clashmonster.commands;

import fr.clashmonster.ClashMonster;
import fr.clashmonster.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetPosSpawnCommand implements CommandExecutor {

    private ClashMonster main;

    public SetPosSpawnCommand(ClashMonster main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        if(commandSender instanceof Player) {
            if (commandSender.hasPermission("core.setposcommand")) {

                Player player = (Player) commandSender;

                if(args.length == 1) {

                    if(args[0].equals("1")) {

                        this.main.getConfig().set("PosX_player1", player.getLocation().getX());
                        this.main.getConfig().set("PosY_player1", player.getLocation().getY());
                        this.main.getConfig().set("PosZ_player1", player.getLocation().getZ());

                        player.sendMessage(Utils.LINE);
                        player.sendMessage("§eVous venez de définir la position du point de spawn du §eJoueur §a§l1");
                        player.sendMessage(Utils.LINE);
                    }

                    if(args[0].equals("2")) {

                        this.main.getConfig().set("PosX_player2", player.getLocation().getX());
                        this.main.getConfig().set("PosY_player2", player.getLocation().getY());
                        this.main.getConfig().set("PosZ_player2", player.getLocation().getZ());
                        player.sendMessage(Utils.LINE);
                        player.sendMessage("§eVous venez de définir la position du point de spawn du §eJoueur §a§l2");
                        player.sendMessage(Utils.LINE);

                    } else if(args.length != 1) {
                        player.sendMessage("§cMauvais Arguments !");
                    }
                }
            }
        }
        return false;
    }
}
