package fr.clashmonster.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HUBCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Player player = (Player) sender;

        if(!(sender instanceof Player))
            return false;

        if(sender instanceof Player) {

           player.teleport(Bukkit.getWorld("world").getSpawnLocation());
           player.sendMessage("§eVous venez d'être téléporté au HUB !");
        }

        return false;
    }
}
