package fr.clashmonster.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            if (commandSender.hasPermission("core.setspawn")) {
                Player player = (Player) commandSender;

                if (!player.getWorld().getName().equalsIgnoreCase("world")) return false;

                player.getWorld().setSpawnLocation(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ());
                player.sendMessage("§eVous venez de modifier le point de spawn du Lobby !");
            }
        }
        return false;
    }
}
